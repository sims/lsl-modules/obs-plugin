OBS plugin
==========

This plugin implements an OBS filter which continuously sends the frame number of the recorded video to LSL in order to synchronize the video with other signals.

## Compiling
### Prerequisites
- CMake
- [liblsl](https://github.com/sccn/liblsl/releases/) (we have tested with version 1.13.1)
- a working development environment for OBS Studio installed on your computer.

### Windows
1. Build obs-studio following the [instructions](https://obsproject.com/wiki/install-instructions#windows-build-directions)
2. clone this repository
3. make a directory called build in the root of this repository
4. launch cmake-gui
  - set the source to the root of this repository
  - set the build to the build directory
  - you'll have to set these CMake entries (the ones in bold must be entered manually, the others will show up when you press "configure" although you may need to do so after each time the configuration progresses)
    - **LSL\_INSTALL\_ROOT** (path) : location of liblsl directory
    - FREETYPE\_INCLUDE\_DIR (path) : in the <obsdependencies>/win64/include/freetype/ (the ones retrieved to build OBS studio)
    - FREETYPE\_INCLUDE_DIR\_ft2build (path) : in the <obsdependencies>/win64/include/ (the ones retrieved to build OBS studio)
    - FREETYPE\_LIBRARY\_RELEASE (filepath) : in the <obsdependencies>/win64/bin/freetype.lib (the ones retrieved to build OBS studio)
    - LIBOBS\_DIR (path) : location of the libobs subfolder in the build folder of OBS studio source
    - **LIBOBS\_INCLUDE\_DIR**: location of the libobs subfolder in root of OBS studio source
    - OBS\_FRONTEND\_LIBRARY (filepath) : location of the obs-frontend-api.lib file
    - **OBS\_PLUGIN\_DIR**: location where to install the plugin (should be C:/ProgramFiles/obs-studio/plugins/64bit/ )
    
    
5. configure, and if everything is okay then generate the build files
6. use the build tool you selected in cmake-gui to compile the code

KNOWN ISSUES: visual studio error LNK1181 cannot open some .lib files (obs-frontend-api.lib and freetype.lib). Problem is that the paths to these libraries are not well defined for visual studio after cmake-gui configuration (probably can be fixed in the cmakelists.txt file in the future).
Temporary solution: In visual studio go to the project properties -> Linker -> Input and edit the "Additional Dependencies" such that the two offending libraries have the full path.

#### installation
copy `lsl-obs-plugin.dll`, `data/Roboto-Black.tff`, `data/text-default.effect` `liblsl64.dll` to the `OBS_PLUGIN_DIR`

### Linux
On Debian/Ubuntu :  
```
git clone https://gitlab.unige.ch/sims/lsl-modules/obs-plugin.git
cd obs-plugin
mkdir build && cd build
cmake -DLIBOBS_INCLUDE_DIR="<path to the libobs sub-folder in obs-studio's source code>" -DOBS_PLUGIN_DIR="<plugin installation path>" -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
```
normally, `OBS_PLUGIN_DIR` in linux should be `/usr/lib/obs-plugins/`
#### installation
```
sudo make install
```
or alternatively, copy `lsl-obs-plugin.so`, `data/Roboto-Black.tff`, `data/text-default.effect` to the `OBS_PLUGIN_DIR`

## Using the plugin
This plugin implements a source in obs. To use, add the LSL plugin source and adjust settings as desired.
Note that if the "autostart" option is selected, manually starting/stopping the recording or streaming in OBS  will not work as expected. This option should be used only if the recording should start/stop synchronously with the start/stop of recording from the LabRecorder (or other LSL recorder).
To create the LSL outlet, you must select the "link" option. Make sure that the ID (and name) of the outlet is unique.