/* Rose Defossez */

/* LLS-Plugin */
#include <obs-module.h>
#include "obs-frontend-api.h"
#include <obs.h>
//#include "lsl_filter.h"
#include <stdio.h>
#include <obs.h>
#include <algorithm>
#include <chrono>
#include <obs-frontend-api.h>
#include <cmath>
#include <lsl_plugin.h>

#include FT_FREETYPE_H
#include <ft2build.h>
#include <sys/stat.h>
#include "obs-convenience.h"
#include "util/platform.h"

#ifdef OBS_PLUGIN_DIR
    #define FONTFILE OBS_PLUGIN_DIR "/Roboto-Black.ttf"
    #define EFFECTFILE OBS_PLUGIN_DIR "/text_default.effect"
#else
    #define FONTFILE "Roboto-Black.ttf"
    #define EFFECTFILE "text_default.effect"
#endif

#define S_RECORD "Recorded"
#define S_STREAM "Streamed"
#define S_CNTLIST "cntlist"
#define S_AUTOSTART "autostart"
#define S_NAME  "name"
#define S_ID "id"
#define S_START "start"
#define S_STOP "stop"
#define S_LINK "link"
#define S_SHOWFRAME "showframe"
#define S_SHOWTS "showts"
#define S_FONTSIZE "fontsize"
#define S_COLOR "color"
#define S_OUTLINE "outline"
#define S_SHADOW "shadow"

#define T_RECORD "Recorded"
#define T_STREAM "Streamed"
#define T_CNTLIST "Frames to count"
#define T_AUTOSTART "Automatically start/stop"
#define T_NAME  "Name"
#define T_ID "Id"
#define T_START "Start"
#define T_STOP "Stop"
#define T_LINK "Link"
#define T_SHOWFRAME "Show frame"
#define T_SHOWTS "Show system time"
#define T_FONTSIZE "Font size"
#define T_COLOR "Text color"
#define T_OUTLINE "Text outline"
#define T_SHADOW "Text shadow"


FT_Library ft2_lib;
OBS_DECLARE_MODULE()

//OBS_MODULE_USE_DEFAULT_LOCALE("lsl_plugin", "en-US")

MODULE_EXPORT const char* obs_module_description(void)
{
    return "LSL plugin source";
}

uint32_t texbuf_w = 2048, texbuf_h = 2048;

bool plugin_initialized = false;

bool init_font(struct lsl_plugin* srcdata)
{
    FT_Long index = 0;
    
    return FT_New_Face(ft2_lib, FONTFILE , index, &(srcdata->font_face)) == 0;
}

static bool lsl_plugin_stop(void* data)
{
    blog(LOG_INFO, "Destroying LSL outlet");
    lsl_plugin* plugin = (lsl_plugin*)data;
    if (plugin->outlet == NULL) {
        blog(LOG_INFO, "no LSL outlet to stop");
        return false;
    }

    plugin->outlet->~stream_outlet();
    plugin->outlet = NULL;
    blog(LOG_INFO, "LSL outlet destroyed");
    return true;
}

static bool lsl_plugin_start(void* data)
{
    lsl_plugin* plugin = (lsl_plugin*)data;
    blog(LOG_INFO, "Creating LSL outlet: %s %s", plugin->name,plugin->id_name);
    
    if (plugin->outlet != NULL) {
        if (plugin->outlet->have_consumers()) {
            blog(LOG_INFO, "LSL outlet already exists and has consumers");
            return false;
        }
        lsl_plugin_stop(data);
    }

    double framerate = video_output_get_frame_rate(obs_get_video());
    //double framerate = lsl::IRREGULAR_RATE; //TODO consider using this to prevent LSL from interpolating data.

    lsl::stream_info info(plugin->name, "OBS frame numbers", 3, framerate, lsl::cf_double64, plugin->id_name);
    lsl::xml_element chns = info.desc().append_child("channels");
    chns.append_child("channel").append_child_value("label", "frame_time").append_child_value("unit", "seconds").append_child_value("type", "timestamp");
    chns.append_child("channel").append_child_value("label", "frame_num").append_child_value("unit", "integer").append_child_value("type", "frameID");
    chns.append_child("channel").append_child_value("label", "frame_dtime").append_child_value("unit", "seconds").append_child_value("type", "time_diff");
    plugin->outlet = new lsl::stream_outlet(info);
    blog(LOG_INFO, "Created LSL outlet");

    return true;
}


static void lsl_plugin_destroy(void* data) {
    struct lsl_plugin* srcdata = (lsl_plugin*)data;

    lsl_plugin_stop(srcdata);

    if (srcdata->font_face != NULL) {
        FT_Done_Face(srcdata->font_face);
        srcdata->font_face = NULL;
    }

    for (uint32_t i = 0; i < num_cache_slots; i++) {
        if (srcdata->cacheglyphs[i] != NULL) {
            bfree(srcdata->cacheglyphs[i]);
            srcdata->cacheglyphs[i] = NULL;
        }
    }

    if (srcdata->text != NULL)
        bfree(srcdata->text);
    if (srcdata->texbuf != NULL)
        bfree(srcdata->texbuf);
    if (srcdata->colorbuf != NULL)
        bfree(srcdata->colorbuf);

    obs_enter_graphics();

    if (srcdata->tex != NULL) {
        gs_texture_destroy(srcdata->tex);
        srcdata->tex = NULL;
    }
    if (srcdata->vbuf != NULL) {
        gs_vertexbuffer_destroy(srcdata->vbuf);
        srcdata->vbuf = NULL;
    }
    if (srcdata->draw_effect != NULL) {
        gs_effect_destroy(srcdata->draw_effect);
        srcdata->draw_effect = NULL;
    }

    obs_leave_graphics();

    bfree(srcdata);
}

extern obs_properties_t* lsl_plugin_properties(void* data) {
    //lsl_plugin* plugin_data = (lsl_plugin*)data;
    //UNUSED_PARAMETER(data);

    //if (plugin_data->props == NULL) {
    obs_properties_t* props = obs_properties_create();

    obs_property_t* prop = obs_properties_add_list(props, S_CNTLIST, T_CNTLIST, OBS_COMBO_TYPE_LIST, OBS_COMBO_FORMAT_STRING);
    obs_property_list_add_string(prop, S_RECORD, T_RECORD);
    obs_property_list_add_string(prop, S_STREAM, T_STREAM);

    obs_properties_add_bool(props, S_AUTOSTART, T_AUTOSTART);
    obs_properties_add_text(props, S_NAME, T_NAME, OBS_TEXT_DEFAULT);
    obs_properties_add_text(props, S_ID, T_ID, OBS_TEXT_DEFAULT);

    obs_properties_add_bool(props, S_SHOWFRAME, T_SHOWFRAME);
    obs_properties_add_bool(props, S_SHOWTS, T_SHOWTS);
    obs_properties_add_int(props, S_FONTSIZE, T_FONTSIZE, 1, 100, 1);
    obs_properties_add_color(props, S_COLOR, T_COLOR);
    obs_properties_add_bool(props, S_OUTLINE, T_OUTLINE);
    obs_properties_add_bool(props, S_SHADOW, T_SHADOW);

    obs_properties_add_bool(props, S_LINK, T_LINK);
    //obs_property_set_modified_callback2(obs_properties_add_bool(props, S_LINK, T_LINK), setting_link_modified, data);

    //    plugin_data->props = props;
   // }


    return  props;
}

static bool setting_link_modified(void *data, obs_properties_t* ppts, obs_property_t* p, obs_data_t* settings)
{
    struct lsl_plugin* plugin_data = (lsl_plugin*)data;

    const char* id_name = obs_data_get_string(settings, S_ID);
    const char* name = obs_data_get_string(settings, S_NAME);

    plugin_data->id_name = (char*)id_name;
    plugin_data->name = (char*)name;

    if (obs_data_get_bool(settings, S_LINK)) {
        lsl_plugin_start(data);
    }
    else {
        lsl_plugin_stop(data);
    }

    /* return true to update property widgets, false
       otherwise */
    return true;
}

static void lsl_plugin_update(void* data, obs_data_t* settings)
{
    
    lsl_plugin* plugin_data = (lsl_plugin*)data;

    const char* id_name = obs_data_get_string(settings, S_ID);
    const char* name = obs_data_get_string(settings, S_NAME);

    plugin_data->id_name = (char*)id_name;
    plugin_data->name = (char*)name;

    if (obs_data_get_bool(settings, S_LINK)) {
        lsl_plugin_start(data);
    }
    else {
        lsl_plugin_stop(data);
    }
    
    const char* cntF = obs_data_get_string(settings, S_CNTLIST);
    if (cntF == std::string(T_STREAM)) {
        plugin_data->record = false;
        plugin_data->stream = true;
    }
    else {
        plugin_data->record = true;
        plugin_data->stream = false;
    }

    plugin_data->autostart = obs_data_get_bool(settings, S_AUTOSTART);
    if (plugin_data->autostart) {
        // disable start/stop record/stream
    }
    else {
        // enable start/stop record/stream
    }

    if (obs_data_get_bool(settings, S_SHOWFRAME) || obs_data_get_bool(settings, S_SHOWTS) ) {
        
        int font_size = obs_data_get_int(settings, S_FONTSIZE);
        bool vbuf_needs_update = false;

        uint32_t color[2];
 

        plugin_data->outline_width = 0;

        plugin_data->drop_shadow = obs_data_get_bool(settings, S_SHADOW);
        plugin_data->outline_text =  obs_data_get_bool(settings, S_OUTLINE);

        if (plugin_data->outline_text && plugin_data->drop_shadow)
            plugin_data->outline_width = 6;
        else if (plugin_data->outline_text || plugin_data->drop_shadow)
            plugin_data->outline_width = 4;

        
        color[0] = (uint32_t)obs_data_get_int(settings, S_COLOR);
        color[1] = (uint32_t)obs_data_get_int(settings, S_COLOR);


        if (color[0] != plugin_data->color[0] || color[1] != plugin_data->color[1]) {
            plugin_data->color[0] = color[0];
            plugin_data->color[1] = color[1];
            vbuf_needs_update = true;
        }

        if (ft2_lib == NULL) {
            obs_data_set_bool(settings, S_SHOWFRAME, false);
            obs_data_set_bool(settings, S_SHOWTS, false);
            plugin_data->show_frame= false;
            plugin_data->show_ts = false;
            return;
        }
            
        
        const size_t texbuf_size = (size_t)texbuf_w * (size_t)texbuf_h;

        if (plugin_data->draw_effect == NULL) {
            blog(LOG_WARNING, " LSL plugin draw effect is null");
            char* error_string = NULL; 

            obs_enter_graphics();
            plugin_data->draw_effect = gs_effect_create_from_file(
                EFFECTFILE, &error_string);
            obs_leave_graphics();

            if (error_string != NULL)
                bfree(error_string);
            
        }

        
        if (plugin_data->font_size != font_size)
            vbuf_needs_update = true;

        const bool new_aa_setting = true;
        const bool aa_changed = plugin_data->antialiasing != new_aa_setting;
        if (aa_changed) {
            plugin_data->antialiasing = new_aa_setting;
            if (plugin_data->texbuf != NULL) {
                memset(plugin_data->texbuf, 0, texbuf_size);
            }
            cache_standard_glyphs(plugin_data);
        }

        if (plugin_data->font_face != NULL) {
            //if (font_size == plugin_data->font_size)
                //goto skip_font_load;

            plugin_data->max_h = 0;
            vbuf_needs_update = true;
        }

        plugin_data->font_size = font_size;
        
        if (!init_font(plugin_data) || plugin_data->font_face == NULL) {
            blog(LOG_WARNING, "FT2-text: Failed to load font ");
            obs_data_set_bool(settings, S_SHOWFRAME, false);
            obs_data_set_bool(settings, S_SHOWTS, false);
            plugin_data->show_frame = false;
            plugin_data->show_ts = false;
            return;
        }
        else {
            if(FT_Set_Pixel_Sizes(plugin_data->font_face, 0, plugin_data->font_size)!=0)
                blog(LOG_WARNING,"problem setting text size");
            if (FT_Select_Charmap(plugin_data->font_face, FT_ENCODING_UNICODE)!=0)
                blog(LOG_WARNING, "problem setting char map");
        }
        if (plugin_data->texbuf != NULL) {
            bfree(plugin_data->texbuf);
            plugin_data->texbuf = NULL;
        }
        plugin_data->texbuf = (uint8_t*)bzalloc(texbuf_size);

        if (plugin_data->font_face)
            cache_standard_glyphs(plugin_data);

    skip_font_load:;

        if (obs_data_get_bool(settings, S_SHOWFRAME)) {
            plugin_data->show_frame = true;
        }
        if (obs_data_get_bool(settings, S_SHOWTS)) {
            plugin_data->show_ts = true;
        }
        
    
    }
    if  ( (!obs_data_get_bool(settings, S_SHOWFRAME) && plugin_data->show_frame)  || (!obs_data_get_bool(settings, S_SHOWTS) && plugin_data->show_ts) ) {
        
        if (!obs_data_get_bool(settings, S_SHOWFRAME)) {
            plugin_data->show_frame = false;
        }
        
        if (!obs_data_get_bool(settings, S_SHOWTS)) {
            plugin_data->show_ts = false;
        }

        if (!plugin_data->show_ts && !plugin_data->show_frame) {
            /*
            for (uint32_t i = 0; i < num_cache_slots; i++) {
                if (plugin_data->cacheglyphs[i] != NULL) {
                    bfree(plugin_data->cacheglyphs[i]);
                    plugin_data->cacheglyphs[i] = NULL;
                }
            }
        
        
            if (plugin_data->texbuf != NULL)
                bfree(plugin_data->texbuf);
            if (plugin_data->colorbuf != NULL)
                bfree(plugin_data->colorbuf);
            */
            obs_enter_graphics();

            if (plugin_data->tex != NULL) {
                gs_texture_destroy(plugin_data->tex);
                plugin_data->tex = NULL;
            }
            if (plugin_data->vbuf != NULL) {
                gs_vertexbuffer_destroy(plugin_data->vbuf);
                plugin_data->vbuf = NULL;
            }
        
            if (plugin_data->draw_effect != NULL) {
                gs_effect_destroy(plugin_data->draw_effect);
                plugin_data->draw_effect = NULL;
            }

            obs_leave_graphics();
        }
        
    }
    

}

extern const char* lsl_plugin_name(void* unused)
{
    UNUSED_PARAMETER(unused);
    return "LSL plugin";// obs_module_text("LSL plugin");
}

static void lsl_plugin_video(void* data, gs_effect_t* effect) {
    UNUSED_PARAMETER(effect);
    lsl_plugin* srcdata = (lsl_plugin*)data;

    if (srcdata == NULL)
        return;

    if ((srcdata->show_frame || srcdata->show_ts)) {
        if (srcdata->tex == NULL || srcdata->vbuf == NULL)
            return;
        if (srcdata->text == NULL || *srcdata->text == 0)
            return;

        gs_reset_blend_state();
        
        if (srcdata->outline_text)
            draw_outlines(srcdata);

        if (srcdata->drop_shadow)
            draw_drop_shadow(srcdata);
        

        draw_uv_vbuffer(srcdata->vbuf, srcdata->tex, srcdata->draw_effect,
            (uint32_t)wcslen(srcdata->text) * 6);
    }
    
}

static void lsl_plugin_tick(void* data, float seconds) {
    lsl_plugin* f = (lsl_plugin*)data;

    if (f == NULL)
        return;


    if ((obs_frontend_recording_active() && f->record) || (obs_frontend_streaming_active() && f->stream)) {
        // we add one to the frame number
        if (obs_frontend_recording_paused() && f->record) {

        }
        else {
            f->frame_number++;
        }
        
    }
    else {
        f->frame_number = 0;
    }

    f->frame_time = (double)((double)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) / 1000);

    if (f->outlet != NULL) {
        
        //obs_output_t* output = obs_frontend_get_recording_output();

        if (f->autostart){
            if (f->outlet->have_consumers()){
                if (f->record && !obs_frontend_recording_active()) {
                    obs_frontend_recording_start();
                }
                if (f->stream && !obs_frontend_streaming_active()) {
                    obs_frontend_streaming_start();
                }

            }
            else {
                if (f->record && obs_frontend_recording_active()) {
                    obs_frontend_recording_stop();
                }
                if (f->stream && obs_frontend_streaming_active()) {
                    obs_frontend_streaming_stop();
                }
            }
        }

        double sample[3];

        sample[0] = f->frame_time;
        sample[1] = (double)f->frame_number;
        sample[2] = seconds;

        // we send the sample to lsl
        f->outlet->push_sample(sample);

    }
    
    
    if ((f->show_frame || f->show_ts)) {

        std::string txt = "";

        if (f->show_frame) {
            std::string id_str = std::to_string(f->frame_number);
            txt += "Frame: " + id_str + "\n";
        }

        if (f->show_ts) {
            std::string ts_str = std::to_string(f->frame_time);
            txt += "Timestamp: " + ts_str;
        }


        std::wstring wtxt = std::wstring(txt.begin(), txt.end());
        f->text = (wchar_t*)wtxt.c_str();

        cache_glyphs(f, f->text);

        set_up_vertex_buffer(f);
    }
}

static void* lsl_plugin_create(obs_data_t* settings, obs_source_t* context) {

    blog(LOG_INFO, "LSL plugin font path: " FONTFILE);
    blog(LOG_INFO, "LSL plugin effect path: " EFFECTFILE);

    struct lsl_plugin* data = (struct lsl_plugin*) bzalloc(sizeof(struct lsl_plugin));
    data->src = context;

    lsl_plugin_init();

    obs_data_set_default_string(settings, S_NAME, "OBS");
    obs_data_set_default_string(settings, S_ID, "ID1");
    obs_data_set_default_bool(settings, S_SHOWFRAME, false);
    obs_data_set_default_bool(settings, S_SHOWTS, false);
    obs_data_set_default_bool(settings, S_LINK, false);
    obs_data_set_default_int(settings, S_FONTSIZE, 30);
    obs_data_set_default_int(settings, S_COLOR, 0xFFFFFFFF);
    obs_data_set_default_bool(settings, S_OUTLINE, false);
    obs_data_set_default_bool(settings, S_SHADOW, false);

    lsl_plugin_update(data, settings);
    return data;

}



struct obs_source_info  create_plugin_info() {
    struct obs_source_info  plugin_info = {};
    plugin_info.id = "lsl_plugin";
    plugin_info.type = OBS_SOURCE_TYPE_INPUT;
    plugin_info.output_flags = OBS_SOURCE_VIDEO | OBS_SOURCE_CUSTOM_DRAW;
    plugin_info.get_name = lsl_plugin_name;
    plugin_info.create = lsl_plugin_create;
    plugin_info.destroy = lsl_plugin_destroy;
    plugin_info.update = lsl_plugin_update;
    plugin_info.get_properties = lsl_plugin_properties;
    plugin_info.video_render = lsl_plugin_video;
    plugin_info.video_tick = lsl_plugin_tick;
    plugin_info.get_width = lsl_plugin_get_width;
    plugin_info.get_height = lsl_plugin_get_height;

    return plugin_info;
}

void lsl_plugin_init() {
    if (plugin_initialized)
        return;

    // obs_output_t *output = obs_frontend_get_recording_output();
     //obs_encoder_t* encoder = obs_output_get_video_encoder(output);
    FT_Init_FreeType(&ft2_lib);

    if (ft2_lib == NULL) {
        blog(LOG_WARNING, "FT2-text: Failed to initialize FT2.");
        return;
    }
    plugin_initialized = true;
}


uint32_t lsl_plugin_get_width(void* data)
{
    struct lsl_plugin* srcdata = (lsl_plugin*)data;

    return srcdata->cx + srcdata->outline_width;
}

uint32_t lsl_plugin_get_height(void* data)
{
    struct lsl_plugin* srcdata = (lsl_plugin*)data;

    return srcdata->cy + srcdata->outline_width;
}


void obs_module_unload(void){
    if (plugin_initialized) {
        FT_Done_FreeType(ft2_lib);
    }
}


bool obs_module_load(void)
{    
    //obs_frontend_add_event_callback(obsstudio_frontend_event_callback, 0);

    // link fonction to the output
    obs_source_info lsl_plugin = create_plugin_info();

    // register this source
    obs_register_source(&lsl_plugin);
    return true;
}